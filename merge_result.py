from __future__ import division, print_function, absolute_import

import os
from timeit import time
import warnings
import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt
from glob import  glob

def compute_iou(box, boxB):
    """
    Computes IUO between two bboxes in the form [x1,y1,x2,y2]
    """
    if len(box) == 0 or len(boxB) == 0:
        o = 0
    else:
        area1 = box[2] * box[3]
        area2 = boxB[2] * boxB[3]
        face_area = np.minimum(area1, area2)
        bb_test = [box[0], box[1], box[0] + box[2], box[1] + box[3]]
        bb_gt = [boxB[0], boxB[1], boxB[0] + boxB[2], boxB[1] + boxB[3]]
        xx1 = np.maximum(bb_test[0], bb_gt[0])
        yy1 = np.maximum(bb_test[1], bb_gt[1])
        xx2 = np.minimum(bb_test[2], bb_gt[2])
        yy2 = np.minimum(bb_test[3], bb_gt[3])
        w = np.maximum(0., xx2 - xx1)
        h = np.maximum(0., yy2 - yy1)
        wh = w * h
        o = wh / face_area

    #          o = wh / ((bb_test[2]-bb_test[0])*(bb_test[3]-bb_test[1])
    #            + (bb_gt[2]-bb_gt[0])*(bb_gt[3]-bb_gt[1]) - wh)
    return (o)

def process_result(result):

    if isinstance(result, list):
        detections_in = np.array(result)
    else:
        detections_in = result

    detections_out = []
    false_detection=[]
    warning_detection= []

    tracker_ids = []

    frame_indices = detections_in[:, 1].astype(np.int)
    min_frame_idx = frame_indices.astype(np.int).min()
    max_frame_idx = frame_indices.astype(np.int).max()

    for frame_idx in range(min_frame_idx, max_frame_idx + 1):

        mask = frame_indices == frame_idx
        rows = detections_in[mask]

        len_id = len(rows)
        if len_id <= 7:
            for row in rows:
                false_detection.append(row )

            continue

        else:
            track_id = min_frame_idx
            for row in rows:
                frame_id , tracker_id , bbox  =row[0] ,row[1],row[2:6]
                tracker_ids.append(tracker_id)
                trackid = track_id

                x = np.maximum(0, bbox[0])
                y = np.maximum(0, bbox[1])
                w = bbox[2]
                h = bbox[3]
                ration_wh = w / h
                area = w*h
                ratial_wh_img = area / (1080*1920)
                #print(trackid)

                #print(ratial_wh_img)

                if ration_wh > 0.666:
                    warning_detection.append([frame_id,trackid,x,y,w,h])
                    continue

                if w > 300  or h > 500:

                    continue

                if h > 40.0 and w >5.0:


                    detections_out.append([frame_id,trackid,x,y,w,h])
            min_frame_idx+=1

        frame_idx+=1
    return detections_out , warning_detection
def compute_min_w_h(result):
    min_box=[]

    for box in result:
        x1 = box[2]
        y1 = box[3]
        w = box[4]
        h = box[5]

        right_bottom_y = y1+h


        if right_bottom_y <= 300:
            min_box.append(box)

    if len(min_box)==0:
        min_w = 5
        min_h = 35
    else:
        np_min_box=np.array(min_box)
        mean_box = np.mean(np_min_box,axis = 0)
        list_mean_box = mean_box#.to_list()
        min_w,min_h = list_mean_box[-2],list_mean_box[-1]
    return min_w,min_h




def process_result_no_warning(result):

    min_w,min_h = compute_min_w_h(result)

    print(min_w,min_h)

    if isinstance(result, list):
        detections_in = np.array(result)
    else:
        detections_in = result

    detections_out = []
    false_detection=[]


    tracker_ids = []

    frame_indices = detections_in[:, 1].astype(np.int)
    min_frame_idx = frame_indices.astype(np.int).min()
    max_frame_idx = frame_indices.astype(np.int).max()

    for frame_idx in range(min_frame_idx, max_frame_idx + 1):

        mask = frame_indices == frame_idx
        rows = detections_in[mask]

        len_id = len(rows)
        if len_id <=5:
            for row in rows:
                false_detection.append(row )

            continue

        else:
            track_id = min_frame_idx
            for row in rows:
                frame_id , tracker_id , bbox  =row[0] ,row[1],row[2:6]
                tracker_ids.append(tracker_id)
                trackid = track_id

                x = np.maximum(0, bbox[0])
                y = np.maximum(0, bbox[1])
                w = bbox[2]
                h = bbox[3]

                if w > 400:

                    continue

                if h > 40.0 and w >5.0:


                    detections_out.append([frame_id,trackid,x,y,w,h])
            min_frame_idx+=1

        frame_idx+=1
    return detections_out
def py_nms(dets,score, thresh=0.6):

    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]
    scores = score


    areas = (x2 - x1 + 1) * (y2 - y1 + 1)

    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= thresh)[0]
        order = order[inds + 1]
    return keep
def merge_result(result):
    add_warning_detections = []

    true_detections ,warning_detection =process_result(result)
    true_detections_list = true_detections
    true_detections = np.array(true_detections)
    frame_indices = true_detections[:, 0].astype(np.int)

    min_frame_idx = frame_indices.astype(np.int).min()
    max_frame_idx = frame_indices.astype(np.int).max()

    warning_detection=np.array(warning_detection)
    wnframe_indices = warning_detection[:, 0].astype(np.int)
    i=1

    for frame_idx in range(min_frame_idx, max_frame_idx + 1):

        mask = frame_indices == frame_idx
        rows = true_detections[mask]

        wnmask = wnframe_indices == i
        warning_rows = warning_detection[wnmask]

        for warn_box in warning_rows:
            ioulist = []
            for detection in rows:
                wbox = warn_box[2:]
                dbox = detection[2:]
                iou = compute_iou(wbox, dbox)
                ioulist.append(iou)
            if len(ioulist) > 0:
                max_iou = max(ioulist)
            else:
                max_iou = 0

            if max_iou > 0.95:
                continue
            else:
                add_warning_detections.append(warn_box)

        i += 1
    # if len(add_warning_detections)>0:
    #     add_warning_detections = np.array(add_warning_detections)
    #     scores = np.random.randn(len(add_warning_detections))
    #
    #     nmsindex = py_nms(add_warning_detections, scores,0.7)  # 0.4
    #     add_warning_detections = add_warning_detections[nmsindex]
    #     add_warning_detections = add_warning_detections.tolist()

    true_detections_out = true_detections_list + add_warning_detections
    print('remove warning detection',len(result)-len(true_detections_out))
    print ('last out',len(true_detections_out))
    print ('warning detection',len(warning_detection))
    print('origin detection',len(result))

    true_detections_out.sort(key=lambda x: x[0])

    return true_detections_out


def write_result(detection_file,output_files):
    detection_file = detection_file
    detections_in = np.loadtxt(detection_file, delimiter=',')
    true_detections = merge_result(detections_in)

    import codecs

    f = codecs.open(output_files, 'w',encoding="utf-8")

    for row in true_detections:
        print('%d,%d,%.1f,%.1f,%.1f,%.1f' % (
                row[0], row[1], row[2], row[3], row[4], row[5]),file=f)

def fix_reslut_and_out(path , output_file = 'result'):
    test_videos = glob('%s/*.txt' % path)
    for a in test_videos:
        # print(a[-6:-4])
        video_name = a[-6:-4]
        # if video_name == 'b2':
        print('---detecting test video---: %s' % video_name)
        if not os.path.exists(output_file):
            os.makedirs(output_file)
        # video_to_img_path = img_out_path+'/'+video_name
        # if not os.path.exists(save_detection_bbox):
        # os.mkdir(file_path)
        # os.makedirs(save_detection_bbox)
        output_files = output_file + '/' + video_name + '.txt'

        write_result(a, output_files)
def convert(frame):
    underexpose = frame

    equalizeUnder = np.zeros(underexpose.shape, underexpose.dtype)
    equalizeUnder[:, :, 0] = cv2.equalizeHist(underexpose[:, :, 0])
    equalizeUnder[:, :, 1] = cv2.equalizeHist(underexpose[:, :, 1])
    equalizeUnder[:, :, 2] = cv2.equalizeHist(underexpose[:, :, 2])
    return  equalizeUnder
def test():
    detection_file = 'b2.txt'
    detections_in = np.loadtxt(detection_file, delimiter=',')
    #true_detections = merge_result(detections_in)
    true_detections = detections_in
    print('origin detection', len(true_detections))

    import codecs

    f = codecs.open('true_detection_b2_klman.txt', 'w',encoding="utf-8")

    for row in true_detections:
        print('%d,%d,%.1f,%.1f,%.1f,%.1f' % (
                row[0], row[1], row[2], row[3], row[4], row[5]),file=f)

    true_detections=np.array(true_detections)
    frame_indices = true_detections[:, 0].astype(np.int)
    i = 1
    video = cv2.VideoCapture(r'F:\desktop\level2_video/b2.mp4')
    while True:
        ret_val, img = video.read()

        #img= convert(img)

        if ret_val != True:
            break

        mask = frame_indices == i


        rows = true_detections[mask]

        for bbox in rows:
            track_id  = bbox[1]
            bbox=bbox[2:]
            #trackid = bbox[1]
            #cv2.putText(img, str(id), (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, (0, 255, 0), 2)
            cv2.rectangle(img, (int(bbox[0]), int(bbox[1])), (int(bbox[2]+bbox[0]), int(bbox[3]+bbox[1])), (0, 255, 255), 2)
            cv2.putText(img, str(track_id), (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, (0, 255, 255), 2)
        cv2.namedWindow("1", 0)
        cv2.resizeWindow("1", 1920, 1080)
        cv2.imshow("1", img)
        i+=1

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    video.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    test()

    #fix_reslut_and_out(path = './')


